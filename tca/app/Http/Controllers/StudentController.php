<?php

namespace App\Http\Controllers;

use App\Models\Student;
use App\Models\Email;
use App\Models\Phone;
use App\Models\Address;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Alert;
use Validator;


class StudentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $students = Student::paginate(10);
        $gender = [
            'Male' => 'Male',
            'Female' => 'Female',
            'Other' => 'Other'
        ];
        return view('student.index', compact('students', 'gender'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $gender = [
            'Male' => 'Male',
            'Female' => 'Female',
            'Other' => 'Other'
        ];

        $email_types = [
            'main' => 'main',
            'secondary' => 'secondary'
        ];

        $phone_types = [
            'cel' => 'cel',
            'tel' => 'tel',
            'fax' => 'fax'
        ];

        $code_area = [
            '55' => 55,
            '66' => 66,
            '64' => 64,
            '68' => 68,
        ];

        $phone_codes = HTTP::get('http://country.io/phone.json');
        $country_codes = $phone_codes->json();

        return view('student.create', compact('gender', 'email_types', 'phone_types', 'country_codes', 'code_area'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'first_name' => 'required',
            'middle_name' => 'required',
            'last_name' => 'required',
            'gender' => 'required',
            'email' => 'required|email',
            'email_type' => 'required',
            'phone' => 'required|integer',
            'phone_type' => 'required',
            'country_codes' => 'required',
            'address_line' => 'required',
            'zip_postcode' => 'required|integer',
            'city' => 'required',
            'state' => 'required',
        ]);
        if($validator->fails()) {
            return redirect('students')->with('toast_error', $validator->errors());
        }
        else {
            try {
                $student = new Student();
                $student->first_name = $request->input('first_name');
                $student->middle_name = $request->input('middle_name');
                $student->last_name = $request->input('last_name');
                $student->gender = $request->input('gender');
                $student->save();
                $student_id = $student->id;

                if($student) {
                    $temp_email = Email::where('email', $request->input('email'))->pluck('email');
                    if(!$temp_email->isEmpty() && $temp_email[0] == $request->input('email')) {
                        return back()->with('toast_error', 'Email registred previously, try other...');
                    }
                    else {
                        $email = new Email();
                        $email->email = $request->input('email');
                        $email->email_type = $request->input('email_type');
                        $email->student_id = $student_id;
                        $email->save();
    
                        $phone = new Phone();
                        $phone->phone = $request->input('phone');
                        $phone->phone_type = $request->input('phone_type');
                        $phone->country_code = $request->input('country_codes');
                        $phone->area_code = $request->input('area_code');
                        $phone->student_id = $student_id;
                        $phone->save();
    
                        $address = new Address();
                        $address->address_line = $request->input('address_line');
                        $address->city = $request->input('city');
                        $address->zip_postcode = $request->input('zip_postcode');
                        $address->state = $request->input('state');
                        $address->student_id = $student_id;
                        $address->save();
                    }

                    return redirect('students')->with('toast_success', 'Student created succesfully...');
                }
            }
            catch(\Illuminate\Database\QueryException $ex) {
                Student::latest()->delete();
                return back()->with('toast_error', 'Error to create new Student. Please check your form...');
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $phone = Phone::where('student_id', $id)->get();
        $email = Email::where('student_id', $id)->get();
        $address = Address::where('student_id', $id)->get();
        $student = Student::find($id);

        $email_types = [
            'main' => 'main',
            'secondary' => 'secondary'
        ];

        $phone_types = [
            'cel' => 'cel',
            'tel' => 'tel',
            'fax' => 'fax'
        ];

        $code_area = [
            '55' => 55,
            '66' => 66,
            '64' => 64,
            '68' => 68,
        ];

        $phone_codes = HTTP::get('http://country.io/phone.json');
        $country_codes = $phone_codes->json();

        return view('student.show', compact('student', 'phone', 'email', 'address', 'email_types', 'phone_types', 'code_area', 'country_codes'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $student = Student::find($id);

        return view('student.edit', compact('student'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Student $student)
    {
        $validator = Validator::make($request->all(), [
            'first_name' => 'required',
            'middle_name' => 'required',
            'last_name' => 'required',
            'gender' => 'required',
        ]);
        if($validator->fails()) {
            return redirect('students')->with('toast_error', $validator->errors());
        }
        else {
            try{
                Student::whereId($student->id)->update([
                    'first_name' => $request->input('first_name'),
                    'middle_name' => $request->input('middle_name'),
                    'last_name' => $request->input('last_name'),
                    'gender' => $request->input('gender'),
                ]);

                return redirect('students')->with('toast_success', 'Student updated Succesfully...');                
            } catch (\Illuminate\Database\QueryException $ex) {
                return back()->withInput()->with('toast_error', 'Error to upate Student. Please check your info...');
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Student $student)
    {
        try {
            $student = Student::find($student->id)->delete();
            return redirect('students')->with('toast_success', 'Student deleted succefully...');
        } catch(\Illuminate\Database\QueryException $ex) {
            return back()->withInput()->with('toast_error', 'Error to delete student. Check info...');
        }
    }
}
