<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Phone;
use Validator;

class PhoneController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        $validator = Validator::make($request->all(), [
            'student' => 'required',
            'phone' => 'required|integer',
            'phone_type' => 'required',
            'country_codes' => 'required',
        ]);
        if($validator->fails()) {
            return redirect('students')->with('toast_error', $validator->errors());
        }
        else {
            try {
                $full_code = explode(' ', $request->input('country_codes'));
                $phone = new Phone();
                $phone->phone = $request->input('phone');
                $phone->phone_type = $request->input('phone_type');
                $phone->country_code = $full_code[0];
                $phone->area_code = $full_code[1];
                $phone->student_id = $request->input('student');
                $phone->save();

                return redirect('students')->with('toast_success', 'New phone number added successfully...');
            }
            catch(\Illuminate\Database\QueryException $ex) {
                return back()->with('toast_error', 'Error to register new phone. Please check your form...');
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
