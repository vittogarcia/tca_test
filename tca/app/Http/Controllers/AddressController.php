<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Address;
use Validator;

class AddressController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'student' => 'required',
            'address_line' => 'required',
            'zip_postcode' => 'required|integer',
            'city' => 'required',
            'state' => 'required',
        ]);
        if($validator->fails()) {
            return redirect('students')->with('toast_error', $validator->errors());
        }
        else {
            try {
                $address = new Address();
                $address->address_line = $request->input('address_line');
                $address->city = $request->input('city');
                $address->zip_postcode = $request->input('zip_postcode');
                $address->state = $request->input('state');
                $address->student_id = $request->input('student');
                $address->save();

                return redirect('students')->with('toast_success', 'New address added successfully...');
            }
            catch(\Illuminate\Database\QueryException $ex) {
                return back()->with('toast_error', 'Error to register new address. Please check your form...');
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
