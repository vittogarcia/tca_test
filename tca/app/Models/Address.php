<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
    use HasFactory;

    protected $table = 'address';
    protected $fillable = [
        'address_line',
        'city',
        'zip_postcode',
        'state',
        'student_id'
    ];

    public function student() {
        return $this->belongsTo('App\Models\Student', 'student_id');
    }
}
