<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Phone extends Model
{
    use HasFactory;

    protected $table = 'phone';
    protected $fillable = [
        'phone',
        'phone_type',
        'country_code',
        'area_code',
        'student_id'
    ];

    public function student() {
        return $this->belongTo('App\Models\Student', 'student_id');
    }

    public function get_fullphone() {
        return $this->country_code . ' ' . $this->area_code . ' ' . $this->phone; 
    }
}
