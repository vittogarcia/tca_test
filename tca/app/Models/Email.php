<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Email extends Model
{
    use HasFactory;

    protected $table = 'email';
    protected $fillable = [
        'email_type',
        'student_id'
    ];

    public function student() {
        return $this->belongsTo('App\Models\Student', 'student_id');
    }
}
