<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    use HasFactory;
    protected $table = 'student';
    protected $fillable = [
        'last_name',
        'middle_name',
        'first_name',
        'gender',
    ];

    public function email() {
        return $this->hasMany('App\Models\Email', 'student_id');
    }

    public function address() {
        return $this->hasMany('App\Models\Address', 'student_id');
    }

    public function phone() {
        return $this->hasMany('App\Models\Phone', 'student_id');
    }

    public function get_fullname() {
        return ucfirst($this->first_name) . ' ' . ucfirst($this->middle_name) . ' ' . ucfirst($this->last_name);
    }
}
