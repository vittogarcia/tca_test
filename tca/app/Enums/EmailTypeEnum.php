<?php

namespace App\Enums;

use MadWeb\Enum\Enum;

/**
 * @method static EmailTypeEnum MAIN()
 * @method static EmailTypeEnum SECONDARY()
 */
final class EmailTypeEnum extends Enum
{
    const __default = self::MAIN;

    const MAIN = 'main';
    const SECONDARY = 'secondary';
}
