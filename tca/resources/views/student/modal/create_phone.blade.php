<form action="/phones" method="POST"> @csrf
    <input type="hidden" name="student" value="{{ $student->id }}">
    <div class="modal fade" id="ModalCreatePhone" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">{{ __('Add new phone') }} </h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="col-sm-12">
                        <label for="phone">Phone Number</label>    
                        <input type="number" min="0" step="1" name="phone" class="form-control" placeholder="phone number" maxlength="10" required>
                    </div>  
                    <div class="col-sm-12">
                        <label for="phone_type">Phone Type</label>
                        <select name="phone_type" id="phone_type" class="form-control" required>
                            <option value="">Select</option>
                            @foreach($phone_types as $id => $phone)
                            <option value="{{ $id }}">{{ $phone }}</option>
                            @endforeach
                        </select>
                    </div>   
                    <div class="col-sm-12">
                        <label for="country_codes">Country Code</label>
                        <select name="country_codes" id="country_codes" class="form-control" required>
                            <option value="">Select</option>
                            @foreach($country_codes as $id => $code)
                            <option value="{{ $id }} {{ $code }}">{{ $id }} {{ $code }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Close</button>
                    <button class="btn btn-success" type="submit">Save</button>
                </div>
            </div>
        </div>
    </div> 
</form>
