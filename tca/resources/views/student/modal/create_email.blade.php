<form action="/emails" method="POST"> @csrf
    <input type="hidden" name="student" value="{{ $student->id }}">
    <div class="modal fade" id="ModalCreateEmail" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">{{ __('Add new email') }} </h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="col-sm-12">
                        <label for="email">Email</label>
                        <input type="email" name="email" class="form-control" placeholder="mail@mail.com" required>
                    </div>        
                    <div class="col-sm-12">
                        <label for="email_type">Email Type</label>
                        <select name="email_type" id="email_type" class="form-control" required>
                            <option value="">Select</option>
                            @foreach($email_types as $id => $type)
                            <option value="{{ $id }}">{{ $type }}</option>
                            @endforeach
                        </select>
                    </div>  
                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Close</button>
                    <button class="btn btn-success" type="submit">Save</button>
                </div>
            </div>
        </div>
    </div> 
</form>
