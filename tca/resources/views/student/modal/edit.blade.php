<form action="/students/{{ $s->id }}" method="POST"> @csrf @method('PUT')
    <input type="hidden" name="student" value="{{ $s->id }}">
    <div class="modal fade" id="ModalEditStudent_{{ $s->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">{{ __('Edit Student') }} {{ $s->get_fullname() }} </h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="col-sm-12">
                        <label for="first_name">First Name</label>
                        <input type="text" name="first_name" class="form-control" placeholder="first name" value="{{ $s->first_name }}" required>
                    </div>
                    <div class="col-sm-12">
                        <label for="middle_name">Middle Name</label>
                        <input type="text" name="middle_name" class="form-control" placeholder="middle name" value="{{ $s->middle_name }}" required>
                    </div>
                    <div class="col-sm-12">
                        <label for="last_name">Last Name</label>
                        <input type="text" name="last_name" class="form-control" placeholder="last name" value="{{ $s->last_name }}" required>
                    </div>
                    <div class="col-sm-12">
                        <label for="gender">Gender</label>
                        <select name="gender" id="gender" class="form-control" required>
                            <option value="{{ $s->gender }}" selected>{{ $s->gender }}</option>
                            @foreach($gender as $id => $g)
                            @if($s->gender != $id)
                            <option value="{{ $id }}">{{ $g }}</option>
                            @endif
                            @endforeach
                        </select>
                    </div> 
                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Close</button>
                    <button class="btn btn-success" type="submit">Save</button>
                </div>
            </div>
        </div>
    </div> 
</form>
