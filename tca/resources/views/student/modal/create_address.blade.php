<form action="/addresses" method="POST"> @csrf
    <input type="hidden" name="student" value="{{ $student->id }}">
    <div class="modal fade" id="ModalCreateAddress" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">{{ __('Add new address') }} </h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="col-sm-12">
                        <label for="address_line">Address</label>
                        <input type="text" name="address_line" class="form-control" placeholder="address" required>
                    </div>   
                    <div class="col-sm-12">
                        <label for="zip_postcode">Zip Code</label>
                        <input type="numbere" min="0" step="1" name="zip_postcode" class="form-control" placeholder="zip_postcode" required>
                    </div>
                    <div class="col-sm-12">
                        <label for="city">City</label>
                        <input type="text" name="city" class="form-control" placeholder="city" required>
                    </div> 
                    <div class="col-sm-12">
                        <label for="state">State</label>
                        <input type="text" name="state" class="form-control" placeholder="state" required>
                    </div> 
                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Close</button>
                    <button class="btn btn-success" type="submit">Save</button>
                </div>
            </div>
        </div>
    </div> 
</form>
