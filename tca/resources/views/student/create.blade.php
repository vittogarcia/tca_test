@extends('layouts.app')

@section('page_title')
Students
@endsection

@section('content')
<div class="card card-danger card-outline">
    <div class="card-body">
    	<h5>Register New Student</h5>
        <form action="/students" method="POST">@csrf
            <div class="form-group row">
                <div class="col-sm-3">
                    <label for="first_name">First Name</label>
                    <input type="text" name="first_name" class="form-control" placeholder="first name" required>
                </div>
                <div class="col-sm-3">
                    <label for="middle_name">Middle Name</label>
                    <input type="text" name="middle_name" class="form-control" placeholder="middle name" required>
                </div>
                <div class="col-sm-3">
                    <label for="last_name">Last Name</label>
                    <input type="text" name="last_name" class="form-control" placeholder="last name" required>
                </div>
                <div class="col-sm-3">
                    <label for="gender">Gender</label>
                    <select name="gender" id="gender" class="form-control" required>
                        <option value="">Select</option>
                        @foreach($gender as $id => $g)
                        <option value="{{ $id }}">{{ $g }}</option>
                        @endforeach
                    </select>
                </div>  
            </div>
            <div class="form-group row">
                <div class="col-sm-3">
                    <label for="email">Email</label>
                    <input type="email" name="email" class="form-control" placeholder="mail@mail.com" required>
                </div>        
                <div class="col-sm-1">
                    <label for="email_type">Email Type</label>
                    <select name="email_type" id="email_type" class="form-control" required>
                        <option value="">Select</option>
                        @foreach($email_types as $id => $type)
                        <option value="{{ $id }}">{{ $type }}</option>
                        @endforeach
                    </select>
                </div>                     
                <div class="col-sm-3">
                    <label for="phone">Phone Number</label>    
                    <input type="number" min="0" step="1" name="phone" class="form-control" placeholder="phone number" maxlength="10" required>
                </div>  
                <div class="col-sm-1">
                    <label for="phone_type">Phone Type</label>
                    <select name="phone_type" id="phone_type" class="form-control" required>
                        <option value="">Select</option>
                        @foreach($phone_types as $id => $phone)
                        <option value="{{ $id }}">{{ $phone }}</option>
                        @endforeach
                    </select>
                </div>   
                <div class="col-sm-2">
                    <label for="country_codes">Country Code</label>
                    <select name="country_codes" id="country_codes" class="form-control" required>
                        <option value="">Select</option>
                        @foreach($country_codes as $id => $code)
                        <option value="{{ $id }}">{{ $id }} {{ $code }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-sm-2">
                    <label for="code_area">Code Area</label>
                    <input type="number" name="area_code" class="form-control" id="code_area" readonly>
                </div>                  
            </div>     
            <div class="form-group row">
                <div class="col-sm-3">
                    <label for="address_line">Address</label>
                    <input type="text" name="address_line" class="form-control" placeholder="address" required>
                </div>   
                <div class="col-sm-3">
                    <label for="zip_postcode">Zip Code</label>
                    <input type="numbere" min="0" step="1" name="zip_postcode" class="form-control" placeholder="zip_postcode" required>
                </div>
                <div class="col-sm-3">
                    <label for="city">City</label>
                    <input type="text" name="city" class="form-control" placeholder="city" required>
                </div> 
                <div class="col-sm-3">
                    <label for="state">State</label>
                    <input type="text" name="state" class="form-control" placeholder="state" required>
                </div>                                                      
            </div>   
            <div class="form-group">
                <button type="submit" class="btn btn-success">Create</button>
                <a href="{{ url()->previous() }}" class="btn btn-primary">Back</a>
            </div>
        </form>                             
    </div>
</div>
@endsection

@section('scripts')
<script>
    $(document).ready(function() {
        $('#gender').select2({theme:'bootstrap4'});
        $('#email_type').select2({theme:'bootstrap4'});
        $('#phone_type').select2({theme:'bootstrap4'});
        $('#country_codes').select2({theme:'bootstrap4'});
    });
    $('#country_codes').on('change', function(){
        console.log($('#country_codes option:selected').text(), $(this).val())
        code = $('#country_codes option:selected').text().split(" ")[1]
        console.log(code)
        $('#code_area').val($('#country_codes option:selected').text().split(" ")[1]);
    });
</script>
@endsection