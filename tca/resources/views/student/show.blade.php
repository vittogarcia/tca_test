@extends('layouts.app')

@section('css')
<link rel="stylesheet" href="{{ asset('plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}">
@endsection

@section('page_title')
Details of Student
@endsection

@section('content')
<div class="card card-danger card-outline">
  <div class="card">
    <div class="card-body">
        <h5 class="text-center">Full Name: {{ $student->get_fullname() }}</h5>
        <h5 class="text-center">Gender: {{ $student->gender }}</h5>
    </div>
    <div class="card-body">
      <div class="accordion" id="accordionExample">
          <div class="card">
            <div class="card-header" id="headingOne">
              <h2 class="mb-0">
                <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                  Address
                </button>
              </h2>
            </div>
        
            <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
              <div class="card-body">
                  <ul class="list-group list-group-flush">
                      @foreach($address as $a)
                      <li class="list-group-item"><strong>Address Line: </strong> {{ $a->address_line }}</li>
                      <li class="list-group-item"><strong>Zip PostCode: </strong> {{ $a->zip_postcode }}</li>
                      <li class="list-group-item"><strong>City: </strong> {{ $a->city }}</li>
                      <li class="list-group-item"><strong>State: </strong> {{ $a->state }}</li>
                      @endforeach
                    </ul><br>
                    <div class="row">
                      <a class="btn btn-primary" data-toggle="modal" data-target="#ModalCreateAddress" href="#">
                        <i class="fas fa-plus"></i> Add Address
                      </a>
                    </div>
              </div>
            </div>
          </div>
          <div class="card">
            <div class="card-header" id="headingTwo">
              <h2 class="mb-0">
                <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                  Emails
                </button>
              </h2>
            </div>
            <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
              <div class="card-body">
                  <ul class="list-group list-group-flush">
                      @foreach($email as $e)
                      <li class="list-group-item"><strong>Email: </strong> {{ $e->email }}</li>
                      <li class="list-group-item"><strong>Email Type: </strong> {{ $e->email_type }}</li>
                      @endforeach
                    </ul><br>
                    <div class="row">
                      <a class="btn btn-primary" data-toggle="modal" data-target="#ModalCreateEmail" href="#">
                        <i class="fas fa-plus"></i> Add Email
                      </a>
                    </div>
              </div>
            </div>
          </div>
          <div class="card">
            <div class="card-header" id="headingThree">
              <h2 class="mb-0">
                <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                  Phones
                </button>
              </h2>
            </div>
            <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
              <div class="card-body">
                  <ul class="list-group list-group-flush">
                      @foreach($phone as $p)
                      <li class="list-group-item"><strong>Phone: </strong> {{$p->get_fullphone() }}</li>
                      <li class="list-group-item"><strong>Phone Type: </strong> {{ $p->phone_type }}</li>
                      @endforeach
                    </ul><br>
                    <div class="row">
                      <button type="button" data-toggle="modal" data-target="#ModalCreatePhone" class="btn btn-primary"><i class="fas fa-plus"></i> Add Phone</button>
                    </div>
              </div>
            </div>
          </div>
      </div>
    </div>
  </div>
</div>
<a href="{{ url()->previous() }}" class="btn btn-primary">Back</a><br><br>
@include('student.modal.create_phone')
@include('student.modal.create_address')
@include('student.modal.create_email')
@endsection

@section('scripts')
<!-- DataTables -->
<script src="{{ asset('plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>
<script>
    $(document).ready( function () {
        $('#students_table').DataTable({
            "paging": true,
            "ordering": true,
            "pageLength": 10,
            "lengthMenu": [10, 20, 30, 40, 50]
        });
    });       
    $('#ModalCreatePhone').on('show.bs.modal', function (e) {
      $('#country_codes').on('change', function(){
        console.log($('#country_codes option:selected').text(), $(this).val(), $('#code_area').val())
        $('#code_area').val($('#country_codes option:selected').text().split(" ")[1]);
    });
    })
</script>
@endsection