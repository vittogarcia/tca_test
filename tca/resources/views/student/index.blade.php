@extends('layouts.app')

@section('css')
<link rel="stylesheet" href="{{ asset('plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}">
@endsection

@section('page_title')
Students
@endsection

@section('content')
<div class="card card-danger card-outline">
	<div class="card-body">
		<div class="d-flex justify-content-end">
			<a class="btn btn-app" href="{{ url('students/create') }}">
              <i class="fas fa-plus"></i> New Student
            </a>
		</div>
	</div>
	<div class="table-responsive p-2">
		<table class="table table-hover table-sm" id="students_table">
			<thead>
				<tr>
					<th>First Name</th>
                    <th>Middle Name</th>
					<th>Last Name</th>
					<th>Gender</th>
					<th>Details</th>
					<th>Edit</th>
					<th>Delete</th>
				</tr>
			</thead>
			<tbody>
				@foreach($students as $s)
				<tr>
					<td>{{ $s->first_name }}</td>
					<td>{{ $s->middle_name }}</td>
					<td>{{ $s->last_name }}</td>
					<td>{{ $s->gender }}</td>
					<td>
						<a href="/students/{{ $s->id }}" class="btn"><i class="far fa-eye"></i></a>
					</td>
					<td>
						<button type="button" data-toggle="modal" data-target="#ModalEditStudent_{{ $s->id }}" class="btn"><i class="far fa-edit"></i></button>
						@include('student.modal.edit')
					</td>
					<td>
						<a data-toggle="modal" data-target="#deleteModal_{{ $s->id}}" class="btn"><i class="far fa-trash-alt"></i></a>
						<!-- Delete Modal-->
						<div class="modal fade" id="deleteModal_{{ $s->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
							<div class="modal-dialog" role="document">
								<div class="modal-content">
								<form id="delete-form" action="/students/{{ $s->id }}" method="POST" style="display: none;">
									@method('DELETE')
									@csrf
									<input type="hidden" name="student" value="{{ $s->id }}">
								</form>
									<div class="modal-header">
										<h5 class="modal-title" id="deleteModalLabel">Delete Student</h5>
										<button class="close" type="button" data-dismiss="modal" aria-label="Close">
										<span aria-hidden="true">×</span>
										</button>
									</div>
									<div class="modal-body">
										This action will delete student: <strong>{{ $s->get_fullname() }}</strong>, Do you want to proceed??
									</div>
									<div class="modal-footer">
										<button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
										<a class="btn btn-danger" href="" onclick="event.preventDefault();
										document.getElementById('delete-form').submit();">Delete</a>
									</div>
								</div>
							</div>
						</div> 
					</td>
				</tr>
				@endforeach
			</tbody>
		</table>
	</div>	
</div>
@endsection

@section('scripts')
<!-- DataTables -->
<script src="{{ asset('plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>
<script>
    $(document).ready( function () {
        $('#students_table').DataTable({
            "paging": true,
            "ordering": true,
            "pageLength": 10,
            "lengthMenu": [10, 20, 30, 40, 50]
        });
    });       
</script>
@endsection