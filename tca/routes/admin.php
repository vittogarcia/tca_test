<?php

use Illuminate\Support\Facades\Route;
use RealRashid\SweetAlert\Facades\Alert;
use App\Http\Controllers\StudentController;

Route::get('', [StudentController::class, 'index'])->name('index');